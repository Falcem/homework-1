//
//  HWHomework.c
//  Homework1
//
//  Created by Vladislav Grigoriev on 21/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#include "HWHomework.h"
#include "stdlib.h"
#include <math.h>
#include  "time.h"
#include  <string.h>

/*
 * Задание 1. (1 балл)
 *
 * Необходмо реализовать метод, который создаст матрицу и заполнит её случайными числами от -100 до 100.
 *
 * Параметры:
 *      rows - количество строк в матрице;
 *      columns - количество столбцов в матрице;
 *
 * Возвращаемое значение:
 *      Матрица размером [rows, columns];
 */
long** createMatrix(const long rows, const long columns) {
    long **Matrix = NULL;
    int i, j;
    srand (time(NULL));
    
    Matrix = (long**) malloc(rows * sizeof(long*));
    for (i = 0; i < rows; ++i)
        Matrix[i] = (long*) malloc(columns * sizeof(long));

    for(i=0; i<rows; ++i)
        for(j=0; j<columns; ++j)
            Matrix[i][j] = rand() % 201-100; //целочисленный интервал [-100;100]

    return Matrix;
}

/*
 * Задание 2.
 *
 * Необходимо реализовать метод, котрый преобразует входную строку, состоящую из чисел, в строку состоящую из числительных.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      numberString - входная строка (например: "1", "123.456");
 *
 * Возвращаемое значение:
 *      Обычный уровень сложности (1 балл):
 *             Строка, содержащая в себе одно числительное или сообщение о невозможности перевода;
 *             Например: "один", "Невозможно преобразовать";
 *
 *      Высокий уровень сложности (2 балла):
 *              Строка, содержащая себе числительные для всех чисел или точку;
 *              Например: "один", "один два три точка четыре пять шесть";
 *
 *
 */
char* stringForNumberString(const char *numberString) {
    int i;
    char* resString=NULL;
    
    resString = (char*)calloc(strlen(numberString)*6, sizeof(char)); //чтобы избежать realloc-ов на каждом case, расход памяти будет больше незначительно
                                                                     //так как длинна конкантенируемых строк 3-5 char
    resString[0] = '\0';
    
    for (i = 0; i < strlen(numberString); ++i){
        switch((int)numberString[i]){
            case 48:
                strcat(resString, "zero");
                break;
            case 49:
                strcat(resString, "one");
                break;
            case 50:
                strcat(resString, "two");
                break;
            case 51:
                strcat(resString, "three");
                break;
            case 52:
                strcat(resString, "four");
                break;
            case 53:
                strcat(resString, "five");
                break;
            case 54:
                strcat(resString, "six");
                break;
            case 55:
                strcat(resString, "seven");
                break;
            case 56:
                strcat(resString, "eight");
                break;
            case 57:
                strcat(resString, "nine");
                break;
            case 46:
                strcat(resString, "point");
                break;
            default:
                strcat(resString, "Error");
                break;
        }
        strcat(resString, " ");
    }
    
    return resString;
}


/*
 * Задание 3.
 *
 * Необходимо реализовать метод, который возвращает координаты точки в зависимости от времени с начала анимации.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      time - время с начала анимации в секундах (например: 0 или 1.234);
 *      canvasSize - длина стороны квадрата, на котором происходит рисование (например: 386);
 *
 * Возвращаемое значение:
 *      Структура HWPoint, содержащая x и y координаты точки;
 *
 * Особенность: 
 *      Начало координат находится в левом верхнем углу.
 *
 * Обычный уровень сложности (2 балла):
 *      Анимация, которая должна получиться представлена в файле Default_mode.mov.
 *
 * Высокий уровень сложности (3 балла):
 *      Анимация, которая должна получиться представлена в файле Hard_mode.mov.
 *
 * PS: Не обязательно, чтобы ваша анимация один в один совпадала с примером. 
 * PPS: Можно реализовать свою анимацию, уровень сложности которой больше или равен анимациям в задании.
 */
HWPoint pointForAnimationTime(const double time, const double canvasSize) {
    HWPoint Coordinates;
    static int flag=1;
    static float radius=10;
    if(radius>canvasSize / 2.0 || radius<2) flag=-flag;
    if(flag==1){    //при flag==1 увеличения радиуса вращения
        radius+=0.5;
    }
    else{
        radius-=0.5;
    }
    Coordinates.x=canvasSize / 2.0 + radius*sin(time); //стандартное описание движения по окружности, но для X -- Sin(time), чтобы добиться вращения
    Coordinates.y=canvasSize / 2.0 + radius*cos(time); //против часовой, как на демонстрационном видео (оси инвертированы)
   
    return Coordinates;
}

