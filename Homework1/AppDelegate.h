//
//  AppDelegate.h
//  Homework1
//
//  Created by Vladislav Grigoriev on 20/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

